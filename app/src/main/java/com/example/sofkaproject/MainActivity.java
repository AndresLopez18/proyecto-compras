package com.example.sofkaproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements ProductAdapter.productAdapterListener {

    private LinearLayout marketCartLayout;
    private LinearLayout recyclerLayout;
    private Realm realm;
    private RecyclerView recyclerProduct;
    private ProductAdapter productAdapter;
    private List<Product> productsFiltered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.app_name);
        }
        realm = Realm.getDefaultInstance();
        recyclerProduct = findViewById(R.id.recyclerView);
        marketCartLayout = findViewById(R.id.marketCartLayout);
        recyclerLayout = findViewById(R.id.recyclerLayout);
        listSize();
        loadProducts();
        openAddProductActivity();
    }

    public void openAddProductActivity() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddProductActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getProducts();
        runLayoutAnimation(recyclerProduct);
        updateProduct();
    }

    private void loadProducts() {
        List<Product> products = getProducts();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerProduct.setLayoutManager(layoutManager);

        recyclerProduct.addItemDecoration(new DividerItemDecoration(recyclerProduct.getContext(), DividerItemDecoration.VERTICAL));
        productAdapter = new ProductAdapter(this, products, this);
        recyclerProduct.setAdapter(productAdapter);

    }

    private List<Product> getProducts() {
        TextView tvTotalCost = findViewById(R.id.tvTotalCost);
        List<Product> products = realm.where(Product.class).findAll().sort("name");
        int accumulate = 0;
        for (int i = 0; i < products.size(); i++) {
            Product product = products.get(i);
            if (product != null) {
                accumulate = accumulate + (product.getQuantity() * product.getCost());
            }
        }
        tvTotalCost.setText(String.valueOf(accumulate));
        return products;
    }

    @Override
    public void deleteProduct(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.deleteProduct);
        builder.setMessage(R.string.askDeleteProduct);
        builder.setPositiveButton(R.string.affirmativeAnswer, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                realm.beginTransaction();
                Product product = realm.where(Product.class).equalTo("id", id).findFirst();
                String productName = " " + product.getName();
                if (product != null) {
                    product.deleteFromRealm();
                }
                realm.commitTransaction();
                Toast.makeText(MainActivity.this, getString(R.string.succesDelete) + productName, Toast.LENGTH_SHORT).show();
                listSize();
                updateProduct();
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.negativeAnswer, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();

    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    private void listSize() {

        List<Product> products = getProducts();
        if (products.size() == 0) {
            marketCartLayout.setVisibility(View.VISIBLE);
            recyclerLayout.setVisibility(View.GONE);
        } else {
            recyclerLayout.setVisibility(View.VISIBLE);
            marketCartLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void editProduct(String id) {
        Intent intent = new Intent(this, EditProductActivity.class);
        intent.putExtra("productId", id);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getString(R.string.search));
        searchView.onActionViewExpanded();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String filter) {
                productsFiltered = realm.where(Product.class)
                        .beginGroup()
                        .contains("name", filter)
                        .endGroup()
                        .findAll().sort("name");

                if (filter.isEmpty()) {
                    getProducts();

                    productAdapter.setProducts(productsFiltered);
                } else {

                    productAdapter.setProducts(productsFiltered);
                }

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
        finish();
    }

    private void updateProduct() {
        productAdapter.updateProducts(getProducts());
    }
}
