package com.example.sofkaproject;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context context;
    private List<Product> productList;
    private productAdapterListener listener;

    public ProductAdapter(Context context, List<Product> productList, productAdapterListener listener) {
        this.context = context;
        this.productList = productList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View template = inflater.inflate(R.layout.product_template, parent, false);
        return new ViewHolder(template);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Product product = productList.get(position);
        int totalValueProduct = product.getCost() * product.getQuantity();
        holder.tvName.setText(product.getName());
        holder.tvCost.setText(String.valueOf(product.getCost()));
        holder.tvQuantity.setText(String.valueOf(product.getQuantity()));
        holder.tvTotalCost.setText(String.valueOf(totalValueProduct));
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deleteProduct(product.getId());
            }
        });
        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.editProduct(product.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void updateProducts(List<Product> products) {
        this.productList = products;
        notifyDataSetChanged();
    }

    public void setProducts(List<Product> productsFiltered) {
        this.productList = productsFiltered;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvCost, tvQuantity, tvTotalCost;
        private ImageView iv_edit;
        private ImageView iv_delete;

        public ViewHolder(View itemView) {

            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvCost = itemView.findViewById(R.id.tv_cost);
            tvQuantity = itemView.findViewById(R.id.tv_quantity);
            tvTotalCost = itemView.findViewById(R.id.tv_totalCost);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            iv_edit = itemView.findViewById(R.id.iv_edit);

        }

    }

    public interface productAdapterListener {

        void deleteProduct(String id);

        void editProduct(String id);
    }
}
