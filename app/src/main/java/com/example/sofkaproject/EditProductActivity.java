package com.example.sofkaproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import io.realm.Realm;

public class EditProductActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etCost;
    private EditText etQuantity;
    private Button btnSave;

    private Realm realm;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);
        realm = Realm.getDefaultInstance();
        loadViews();
        id = getIntent().getStringExtra("productId");
        loadProduct(id);
    }

    private void loadProduct(String id) {
        Product product = realm.where(Product.class).equalTo("id", id).findFirst();
        if (product != null) {
            etName.setText(product.getName());
            etCost.setText(String.valueOf(product.getCost()));
            etQuantity.setText(String.valueOf(product.getQuantity()));
        }
    }

    private void loadViews() {
        etName = findViewById(R.id.etName);
        etCost = findViewById(R.id.etCost);
        etQuantity = findViewById(R.id.etQuantity);
        btnSave = findViewById(R.id.btnAddProduct);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });
    }

    private void updateProduct() {
        String name = etName.getText().toString();
        String nameReplaceBlanks = name.replaceAll("[\\s]+", " ").trim();
        String cost = etCost.getText().toString().trim();
        String quantity = etQuantity.getText().toString().trim();
        int iCost;
        int iQuantity;

        String message = getString(R.string.required);
        String stringError = getString(R.string.notAllowedString);

        if (name.isEmpty()) {
            etName.setError(message);
        } else if (!name.matches("[A-Za-z0-9á-ú|\\s|ñ]+")) {
            etName.setError(stringError);
        }

        if (cost.isEmpty()) {
            etCost.setError(message);
        }

        if (quantity.isEmpty()) {
            etQuantity.setError(message);
        }

        if (!(name.isEmpty() || cost.isEmpty() || quantity.isEmpty() || !name.matches("[A-Za-z0-9á-ú|\\s|ñ]+"))) {
            iCost = Integer.parseInt(cost);
            iQuantity = Integer.parseInt(quantity);
            Product Product = new Product(id, nameReplaceBlanks, iCost, iQuantity);
            updateProductInDB(Product);
            finish();
        }
    }

    private void updateProductInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
    }

    public void cancel(View view) {
        onBackPressed();
    }

}
