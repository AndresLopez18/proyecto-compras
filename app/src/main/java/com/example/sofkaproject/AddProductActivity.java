package com.example.sofkaproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmQuery;

public class AddProductActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etCost;
    private EditText etQuantity;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        realm = Realm.getDefaultInstance();
        loadViews();
    }

    private void loadViews() {
        etName = findViewById(R.id.etName);
        etCost = findViewById(R.id.etCost);
        etQuantity = findViewById(R.id.etQuantity);
    }

    public void save(View view) {
        String name = etName.getText().toString();
        String nameReplaceBlanks = name.replaceAll("[\\s]+", " ").trim();
        String cost = etCost.getText().toString().trim();
        String quantity = etQuantity.getText().toString().trim();

        int iCost;
        int iQuantity;
        String message = getString(R.string.required);
        String stringError = getString(R.string.notAllowedString);

        if (name.isEmpty()) {
            etName.setError(message);
        } else if (!name.matches("[A-Za-z0-9á-ú|\\s|ñ]+")) {
            etName.setError(stringError);
        }

        if (cost.isEmpty()) {
            etCost.setError(message);
        }

        if (quantity.isEmpty()) {
            etQuantity.setError(message);
        }

        if (!(name.isEmpty() || cost.isEmpty() || quantity.isEmpty() || !name.matches("[A-Za-z0-9á-ú|\\s|ñ]+"))) {
            iCost = Integer.parseInt(cost);
            iQuantity = Integer.parseInt(quantity);
            String id = UUID.randomUUID().toString();
            Product product;
            product = new Product(id, nameReplaceBlanks, iCost, iQuantity);
            buySound();
            openMainActivity(product);
            Toast.makeText(this, R.string.succesAdd, Toast.LENGTH_SHORT).show();
        }

    }

    private void openMainActivity(final Product product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.addProduct);
        builder.setMessage(R.string.askAddProduct);
        builder.setPositiveButton(R.string.affirmativeAnswer, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                addProductInDB(product);
                etName.setText("");
                etCost.setText("");
                etQuantity.setText("");
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.negativeAnswer, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                addProductInDB(product);
                Intent intent = new Intent(AddProductActivity.this, MainActivity.class);
                startActivity(intent);
                dialogInterface.dismiss();
                finish();
            }
        });

        builder.create().show();

    }

    private void buySound() {
        MediaPlayer buyProduct = MediaPlayer.create(this, R.raw.buy_product);
        buyProduct.start();
    }

    private void addProductInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
    }

    public void cancel(View view) {
        onBackPressed();
    }

}
